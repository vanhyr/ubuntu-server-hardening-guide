[#](#) **Ubuntu server hardening guide**

**Language:**

- [🇪🇸 Español](./README.es.md) (work in progress)
- [🇺🇸 English](./README.md)

In this guide I will provide a robust way to install Ubuntu Server and securing it in a basic and
simple way just so you can start deploying your services right away.

With no more further ado, let's begin.

# Table of contents

- **[Installation media](#installation-media)**
  - [Downloading the media creation tool](#downloading-the-media-creation-tool)
  - [Creating the installation media](#creating-the-installation-media)
- **[Initial setup](#initial-setup)**
  - [Disk layout](#disk-layout) TODO
- **[Hardening and security](#hardening-and-security)**
  - [Updating the system](#updating-the-system)
  - [Create non root user](#create-non-root-user)
  - [Unattended upgrades](#unattended-upgrades)
  - [SSH](#ssh)
    - [Openssh server](#openssh-server)
    - [Hardening](#hardening)
      - [Authentication](#authentication)
        - [Client key pair](#client-key-pair)
        - [Server trusted keys](#server-trusted-keys)
      - [SSHD config](#sshd-config)
    - [Client alias](#client-alias)
  - [Firewall](#firewall)
    - [Specific rules](#specific-rules)
      - [SSH](#ssh-1)
      - [Web server](#web-server)
      - [DNS](#dns)
    - [Global rules](#global-rules)
    - [Block ping requests](#block-ping-requests)
  - [Shared memory](#shared-memory)
  - [System control](#system-control)
  - [Host](#host)
  - [Fail2Ban](#fail2ban)
- **[Extra](#extra)**
  - [Programs](#programs)
    - [Apt install](#apt-install)
    - [Manual install](#manual-install)
      - [Exa](#exa)
      - [Powerlevel10k](#powerlevel10k)
    - [Config](#config)
      - [ZSH](#zsh)
  - [Dotfiles](#dotfiles)
    - [Restore](#restore)
    - [Initalize](#initalize)
      - [Branch per host](#branch-per-host)
  - [Samba](#samba)
  - [Docker](#docker)
- **[Deploy with ansible](#deploy-with-ansible) TODO -> Move to another repo?

# Installation media

[###](###) Downloading the media creation tool

You can download the official **media creation tool** from [Ubuntus's official downloads page](https://ubuntu.com/download/server "Ubuntus official downloads page") for free.

- [Download Ubuntu Server 20.04.2 LTS (direct download)](https://releases.ubuntu.com/22.04.2/ubuntu-22.04.2-live-server-amd64.iso "download Ubuntu Server 20.04.4 LTS")

### Creating the installation media

What you'll need to do:

- Grab a **8GB or more** empty USB flash drive (if it isn't empty you'll need to format it)

- **Insert** it into your PC.

    - If the USB flash drive is **empty** you are good to go.

    - If the USB flash drive is not empty I recommend **formatting it** before doing anything.

    > Open file explorer and right click the USB flash drive > select *format...* > and click *start*.

- Now [download](https://www.balena.io/etcher/ "go to download page") and open open Balena Etcher, select the Ubuntu Server ISO and flash it to your USB flash drive.

\* Note that it should **take a while** to flash it into your USB flash drive so be patient.

# Initial setup

### Disk layout

# Hardening and security

If you want your server to be more secure and mitigate most attacks follow along this section. 

> Note that these are the basics and you might want to monitor your instance in order to be aware of possible security risks and better mitigations in the future.

## Updating the system

Make sure your system is up to date after installing.

```sh
sudo apt update && sudo apt upgrade
```

## Create non root user

It is always a good idea to create a separate user instead of only using the root user.

> Note that you should only perform this task if the installer didn't already create a separate account. Or you want another user on your system.

Create the new user:

```sh
sudo useradd -m -g {user} -G sudo,{other_groups} -s {shell} {user}
```

> Command parameters explanation:
>
> - `-m` -> Create the user home directory. It is optional.
> - `-g {user}` -> Add the primary group to the user. It normally it's called the same as the user.
> - `-G` -> Adds secondary groups to the user. 
> - `sudo` -> Being part of the this group (sometimes called `wheel`), means that the users in that group will have administrator privileges while using the `sudo` command. 
> `{other_groups}` -> Refer to a list of comma separated groups that the user might need to belong like docker, samba, media, torrent, etc.
> - `-s {shell}` -> Specifies the login shell for the user. Most popular ones are `bash` and `zsh`.
> - `{user}` -> The username for this account

Uncomment the required line in `/etc/sudoers` in order to specify how to grant those administrative privileges to users in the `sudo`\`wheel` group.

In case of `sudo`:

```vim
%sudo ALL=(ALL:ALL) ALL
```

In case of `wheel`:

```vim
%wheel  ALL=(ALL:ALL) ALL
```

Now if you belong to `wheel` or `sudo` you have privileges to run commands usin the `sudo` command.

## Unattended upgrades

In order to keep the system with the latest security updates, you might want to add unnatended-upgrades to your server instace.

Intall the `unattended-upgrades` package if is not already installed:

```sh
sudo apt install unattended-upgrades
```

Run the initial installation process.

```sh
dpkg-reconfigure --priority=low unattended-upgrades
```

Hit yes to prompt.

### Configuration

Lets make some changes to its config at `/etc/apt/apt.conf.d/50unattended-upgrades`.

```conf
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
Unattended-Upgrade::Remove-New-Unused-Dependencies "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "true";
Unattended-Upgrade::Automatic-Reboot-Time "02:00";
```

## SSH

In order to access your server shell from another computer you'll need to set up SSH (Secure Shell Server). You need to install and enable openssh in the server if it is not present already. Then you'll need to configure the clients to connect to the server instance.

> Note that you'll need the `openssh-client` package installed in your client.

### Openssh server

The server side needs the `openssh-server` package.

```sh
sudo apt install openssh-server
```

After that you'll need to enable the service.

```sh
sudo systemctl enable sshd.service
```

> Note: Skip this process if `openssh-server` is already installed either manually or from the ubuntu setup process.

### Hardening

The SSH protocol is a very common security breach because some people leave the config defaults. In order to ensure that you keep your SSH instance secure, some basic steps are recommended.

1. Create a SSH key pair in your client.
2. Add that keypair public key to your server trusted keys.
3. Change the sshd config to be more robust.

#### Authentication

It is recommended to not use password authentication for SSH. For that reason a key pair based
authentication it's what you want to use.

##### Client key pair

To use SSH keys you want to create the SSH key pair in the client for later alowing that client public key to connect to the server.

Generate the SSH key pair with the following command:

```sh
ssh-keygen -t ed25519 -C "{user@host}"
````

> Command parameters explanation:
>
> - `ssh-keygen` -> Generate a new SSH key pair.
> - `-t ed25519` -> The type of key to create. In this case ed25519 is a modern and secure algorithm.
> - `-C "{user@host}"` -> A comment with the username and the hostname of that client.

You will be prompted to specify a password while generating that new key pair. I recommend setting a password in order to add an extra layer of protection. But it is up to you to add it or not.

The SSH key pair is stored under `./ssh` in your users home directory. It consists of 2 files:

- Private key (`id_ed25519`)-> Do not share this key.
- Public key (`id_ed25519.pub`) -> That key can be shared to grant access to this client into the various servers.

##### Server trusted keys

Now that you generated as many SSH key pairs in your clients as you want it is time to tell the server to accept SSH connections from those trusted clients.

You can either ssh into the server and copy the id manually into the `.ssh/authorized_keys` file.

Or you can copy from the client using:

```sh
ssh-copy-id -i ~\.ssh\id_ed25519.pub {user@host} -p {port}
```

> Command parameters explanation:
>
> - `ssh-copy-id` -> Copy the client public key into the server authorized keys.
> - `{user@host}` -> The user and host of the server SSH connection.
> - `-p {port}` -> The current SSH port (note that it might still be the default 22 port)

#### SSHD config

Change the following lines in `/etc/ssh/sshd_config`:

```conf
Port {port}
AddressFamily inet
PermitRootLogin no
LoginGraceTime 20
PasswordAuthentication no
PermitEmptyPasswords no
UsePAM no
Banner no
```

> I recommend not using a port above 1024 for SSH to avoid security issues.

Optionally you can add this to restrict the users who can login to the SSH session:

```conf
AllowUsers {user}
``` 

> Above config values are explanained here:
>
>  - `Port {port}` -> Use a different port to prevent default port 22 attacks. I recommend not using a port above 1024 for SSH to avoid security issues.
>  - `AddressFamily inet` -> Only allow IPv4 connections.
>  - `PermitRootLogin no` -> Disable root login to prevent giving administrator account to the the attacker.
>  - `LoginGraceTime 20` -> Lower the time to prevent DoS or brute force attacks.
>  - `PasswordAuthentication no` -> Disable plain password authentication, limiting it to ssh keys.
>  - `PermitEmptyPasswords` -> Disable empty passwords.
>  - `UsePAM no` -> Disable PAM (Privileged Access Management)
>  - `Banner no` -> Disable banner, this way the attacker receive no additional info about your
>    system.
>  - `AllowUsers` {user} -> Allow only certain users to login.

Now you can restart the sshd service to apply changes:

> I strongly recommend keeping an extra ssh connection when doing this to stay logged in in case something goes wrong.

```sh
sudo systemctl restart sshd.service
```

Test if the connection works in your clients with the following command:

```sh
ssh -p {port} {user@host}
```

### Client alias

You my want to add an alias to stablish the SSH connection. Add this to your alias file:

```sh
alias {host}="ssh -p {port} {user@host}"
```

> Note that your alias file could be `~/.bashrc`, `~/.zshrc` or a separate sourced file.
> I like to keep my alias file separated and consistent for both bash and zsh shells. I source it in both my `~/.bashrc` and my `~/.zshrc` to `~/.config/shell/aliasrc` so I can use the same aliases in both shells and make changes to just one file.

## Firewall

Firewalling your server is important as you want to control the ports that are exposed in your
machine. We are going to use `ufw` for simplicity but `iptables` works too for defining firewall
rules as `ufw` relays on `iptables` to work.

Install `ufw` if it is not already installed.

```sh
sudo apt install ufw
```

### Specific rules

We are going to define some services rules in here. You can pretty much add as many as you want but remember that you should know what you are allowing and the risks it may cause.

> IMPORTANT:
> Note that docker containers may not be affected by your server ufw specific rules.

#### SSH

Default SSH port is 22 but if you did change the default {port} you can still create a firewall
rule.

```sh
sudo ufw limit {port}/tcp comment "SSH"
```

> Command parameters explanation:
>
> `limit` -> Opens the port but limits the times someone can try to acces that port to 6 times in 30 second to avoid brute force attacks from bots.

> I recommend not using a port above 1024 for SSH to avoid security issues.

#### Web server

Default HTTP port is 80 and default HTTPS port is 443.

```sh
sudo ufw allow 80/tcp comment "HTTP"
sudo ufw allow 443/tcp comment "HTTPS"
```

#### DNS

Default DNS port is 53 in both udp (main) and tcp.

```sh
sudo ufw allow 53/udp comment "DNS"
sudo ufw allow 53/tcp comment "DNS"
```

### Global rules

We are going to globally block all incoming traffic and allow all outgoing traffic. That way only the incoming traffic that we specify under the specific rules can communicate with our server.

```sh
sudo ufw default deny incoming
sudo ufw default allow outgoing
```

We can always allow specific rules for different services.

### Block ping requests

Disable ping requests to this machine.

Edit `/etc/ufw/before.rules`:

```vim
-A ufw-before-input -p icmp --icmp-type echo-request -j DROP
````

Check ufw rules status.

```sh
sudo ufw status numbered
```

> I add the `numbered` parameter to see each rule number in case I want to modify/delete any entry.

You can now apply the new ufw rules and enable ufw.

> I strongly recommend keeping an extra ssh connection when doing this to stay logged in in case something goes wrong.

```sh
sudo ufw enable
```

## Shared Memory

An attack towards  [shared memory](https://help.ubuntu.com/community/StricterDefaults) can run against `/dev/shm`. You surely want to reduce that risk in your server.

Edit `/etc/fstab` and add this:

```
tmpfs /dev/shm tmpfs defaults,noexec,nosuid 0 0
```

## System control

In order to prevent source routing of incoming packets and log malformed IP's, you need to edit `/etc/sysctl.conf` which contains most network settings.

```conf
# IP Spoofing protection
net.ipv4.conf.default.rp_filter=1
net.ipv4.conf.all.rp_filter=1

# Ignore ICMP redirects
net.ipv4.conf.all.accept_redirects = 0
net.ipv6.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0 
net.ipv6.conf.default.accept_redirects = 0

# Ignore send redirects
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0

# Disable source packet routing
net.ipv4.conf.all.accept_source_route = 0
net.ipv6.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv6.conf.default.accept_source_route = 0

# Log Martians
net.ipv4.conf.all.log_martians = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1

# Ignore Directed pings
net.ipv4.icmp_echo_ignore_all = 1

# Block SYN attacks
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_syn_backlog = 2048
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 5

# Ignore ICMP broadcast requests
net.ipv4.icmp_echo_ignore_broadcasts = 1

# Disable ipv6 on the system
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
```

# Max watches per user
fs.inotify.max_user_watches=204800 # mostly for syncthing

To apply the latests changes run:

```sh
sudo sysctl -p
```

## Host

You want to prevent ip spoofing in the `/etc/host.conf` file.

```conf
order bind,hosts
multi on
#nospoof on
```

## Fail2Ban

Fail2ban scans log files and bans IPs that show the malicious signs like too many password failures, seeking for exploits, etc.

Install `fail2ban`.

```sh
sudo apt install fail2ban
```

Enable the service.

```sh
sudo systemctl enable fail2ban
sudo systemctl start fail2ban
```

Edit the config, but firts make a backup.

```sh
sudo cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local
sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
```

Now let's configure `fail2ban` at `/etc/fail2ban/jail.local`.

```
ignoreip = 127.0.0.1/8 ::1 {address}
maxretry = 2
[sshd]
port = {port}
```

Restart the `fail2ban` service to apply changes.

```sh
sudo systemctl restart fail2ban
```

> You can check the status by running: `sudo fail2ban-client status sshd`

# Extra

Some extra steps to follow after securing your server.

## Programs

The list of useful programs for managing the server.

- `zsh` -> The ZShell
- `bat` -> A modern alternative to `cat` with more features.
- `neofetch` -> System info output.
- `neovim` -> A modern `vim` with extended functionallity.
- `zsh-autosuggestions` -> A plugin for `zsh` to enable auto suggestions.
- `zsh-syntax-highlighting` -> A plugin for `zsh` to enable syntax highlihgting.
- `exa` -> A modern `ls` with more features. [Manual install]
- `powerlevel10k` -> A `zsh` theme. [Manual install]

### Apt install

```sh
sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting neovim bat neofetch
```

### Manual install

#### Exa

Install exa from the github repo.

```sh
wget -c http://old-releases.ubuntu.com/ubuntu/pool/universe/r/rust-exa/exa_0.9.0-4_amd64.deb
sudo apt install ./exa_0.9.0-4_amd64.deb
rm exa_0.9.0-4_amd64.deb
```

#### Powerlevel10k

Manually install using the github repo.

```sh
sudo git clone --depth=1 https://github.com/romkatv/powerlevel10k.git /usr/share/zsh-theme-powerlevel10
```

### Setup

#### ZSH

Change the user shell to `zsh`.

```sh
sudo chsh {user} -s /bin/zsh
```

## Dotfiles

The easiest way you can manage your server dotfiles is using git bare repos. You can restore an existing one or just set up and initialize the repo to keep track of your dotfiles.

You probably want to keep track of these dotfiles, so you may want to add these to a `.gitignore`:

- `~/.config` -> the location where your programs usually stored their config files. You may want to add a separate `.gitignore` inside that folder.
- `~/.local` -> for the bin scripts and some other stuff. You may want to add a separate `.gitignore` inside that folder.
- `~/.bashrc` -> the config for bash.
- `~/.bash_logout` -> a file that clears the console when you exit bash for security and privacy reasons.
- `~/.zshenv` -> the config for zsh.
- `~/.hushlogin` -> a file that supresses the login message of your server.

### Restore

In order to restore an existing git bare repo you want to:

Generate a new ssh key if not already present.

```sh
ssh-keygen -t ed25519 -C "{user@host}"
```

Add that key to your `gitlab` or `github` account.

Create a file called `resdot` in `~` and add the following lines:

```sh
#!/bin/sh
git clone --bare {repo_ssh_url} $HOME/.local/.dot
function dot {
  /usr/bin/git --git-dir=$HOME/.local/.dot/ --work-tree=$HOME $@
}
dot checkout
if [ $? = 0 ];
then
  echo "Checked out dotfiles.";
else
  echo "Backing up pre-existing dotfiles.";
  mkdir -p $HOME/.local/.dot-bak
  dot checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} $HOME/.local/.dot-bak/{}
fi;
dot checkout
dot config status.showUntrackedFiles no
```

> {repo_ssh_url} -> the SSH clone url of your repo from `gitlab` or `github`.

Then you want to give the script execute permissions.

```sh
sudo chmod +x {script_name}
```

> {script_name} -> could be `resdot`, `resdot-ssh` or whatever you named yours.

When you run the script it should place your bare repo at `~/.local/.dot` and start tracking files
specified into a `.gitignore` inside your home directory.

The `.gitignore` should look like this:

```.gitignore
# Ignore everything
/*
!.gitignore

# Except for what we want to track
!.bashrc
!.bash_logout
!.zshenv
!.config
!.local
!.hushlogin
```

Now you may want to add `dot` as an alias to your `alias` file.

```sh
alias dot='/usr/bin/git --git-dir=$HOME/.local/.dot --work-tree=$HOME'
```

You can now delete the script or just move it to `~/.local/bin`.

```sh
# Delete
rm -rf {script_name}

# Move
mv {script} ~/.local/bin
```

Now you can start using it normally like below to store your dotfiles:

```sh
# Add changes
dot add .

# Commit staged changes
dot commit -m "{message}"

# Change branch and checkout in case you want to store different servers dotfiles
dot branch
dot checkout

# Push changes to the git repo
dot push
```

> Above are just some examples. As you can see you can manage it like if it was a normal git repo using `git` but using the `dot` command instead.

### Initalize

Initalizing a git bare repo for storing dotfiles is simple.

```sh
git init --bare $HOME/.local/.dot
alias dot='/usr/bin/git --git-dir=$HOME/.local/.dot/ --work-tree=$HOME'
dot config --local status.showUntrackedFiles no
dot config --local init.defaultBranch main
dot branch -m main
```

Now create a new empty repo in `gitlab` or `github`. Then add the remote to store your dotfiles.

```sh
dot remote add gitlab {ssh_clone_url}
```

> `ssh_clone_url` -> May look like this: `git@gitlab.com:{user}/{repo_name}.git`

Now you can start tracking files using a `.gitignore` file like this:

```.gitignore
# Ignore everything
/*
!.gitignore

# Except for what we want to track
!.bashrc
!.bash_logout
!.zshenv
!.config
!.local
!.hushlogin
```

After that you can just add the files to the staging area.

```sh
dot add .
```

Commit the staged changes.

```sh
dot commit -m "Initial dotfiles commit"
```

And finally just push to the remote.

```sh
dot push -u {remote} main
```

#### Branch per host

Aditionally, if you want to add a branch for one specific server hos host.

```sh
dot checkout -b {host} main
```

Now you can make changes and add them.

```sh
dot add .
```

Commit the staged changes.

```sh
dot commit -m "{message}"
```

And finally you can push to your new branch.

```sh
dot push -u {remote} {host}
```

## Docker

Intall docker for isolation, modularity and easier management of services.

Firts set up the `docker` repo.

```sh
sudo apt update
#sudo apt install ca-certificates curl gnupg lsb-release
#sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Now install the `docker` engine.

```sh
sudo apt update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

Now add your user to the `docker` group and you are good to go.

```sh
sudo usermod -aG docker {user}
```

Configure the docker service by editing `/etc/docker/daemon.json`.

```daemon.json
{
  "bip": "172.17.0.1/16",
  "default-address-pools": [
    {
      "base": "172.16.0.0/16",
      "size": 24
    }
  ],
  "no-new-privileges": true
}
```

> Note that you might want to follow the RFC 1918 specification for local ip ranges (10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16)

> The options mean:
> - `default-address-pools` -> Default address pool for node specific local networks (ip range for custom bridge networks).
> - `no-new-privileges` -> Set no-new-privileges by default for new containers.

Finally restart the `docker` daemon for changes to take effect.

```sh
sudo systemctl restart docker
```
